import unittest
import os
import django

class TestNoWidget(unittest.TestCase):
    def test_setup(self):
        os.environ['DJANGO_SETTINGS_MODULE'] = 'tests.settings_no_widget'
        with self.assertRaises(Exception):
            django.setup()

if __name__ == '__main__':
    unittest.main()
