from django.urls import include, path
from django.conf import settings

if 'ckeditor_uploader' in settings.INSTALLED_APPS:
    urlpatterns = [
        path('ckeditor/', include('ckeditor_uploader.urls')),
    ]
elif 'ckeditor' in settings.INSTALLED_APPS:
    urlpatterns = []
elif 'tinymce' in settings.INSTALLED_APPS:
    urlpatterns = [
        path('tinymce/', include('tinymce.urls')),
    ]
else:
    raise Exception("Cannot find widget to use with richflatpages. Please add 'ckeditor' (perhaps with 'ckeditor_uploader') or 'tinymce' to your INSTALLED_APPS in your Django settings config.")

urlpatterns += [
    path('', include('django.contrib.flatpages.urls')),
]
