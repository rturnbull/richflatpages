# https://gist.github.com/elidickinson/1379652

from django.contrib import admin
from django.db import models
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import gettext_lazy as _
from django.conf import settings

if 'ckeditor_uploader' in settings.INSTALLED_APPS:
    from ckeditor_uploader.widgets import CKEditorUploadingWidget
    widget = CKEditorUploadingWidget
elif 'ckeditor' in settings.INSTALLED_APPS:
    from ckeditor.widgets import CKEditorWidget  
    widget = CKEditorWidget
elif 'tinymce' in settings.INSTALLED_APPS:
    from tinymce.widgets import TinyMCE
    widget = TinyMCE(attrs={'cols': 80, 'rows': 30})
else:
    raise Exception("Cannot find widget for flatpages. Please add 'ckeditor' or 'tinymce' to your installed apps.")


# Define a new FlatPageAdmin
class FlatPageAdmin(FlatPageAdmin):
    formfield_overrides = {
        models.TextField: {
            'widget': widget,
        }
    }

# Re-register FlatPageAdmin
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)
