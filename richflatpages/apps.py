from django.apps import AppConfig


class RichflatpagesConfig(AppConfig):
    name = 'richflatpages'
