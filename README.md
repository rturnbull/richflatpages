# richflatpages

Simple module to use a widget when using Django's flatpages.

You can install it via pip:
```
pip install -e git+https://gitlab.unimelb.edu.au/rturnbull/richflatpages.git#egg=richflatpages
```

# Configuration

You can choose to use richflatpages with TinyMCE or CKEditor. To do so, just include ```"tinymce"``` or ```"ckeditor"``` in the INSTALLED_APPS ahead of ```"richflatpages"```. Also make sure you add in ```"django.contrib.flatpages"```. If you wish to upload files with CKEditor, make sure you also include ```'ckeditor_uploader'```.

For example:
```
INSTALLED_APPS += [
    "django.contrib.sites",
    "django.contrib.flatpages",
    "ckeditor",
    "ckeditor_uploader",
    "richflatpages",
]
```

Then migrate your database:
```
./manage.py migrate
```

Then add the urls to the bottom of your main urls.py:
```
urlpatterns += [
    path('', include('richflatpages.urls')),    
]
```

It includes a basic template with the name `flatpages/default.html`. It presupposes a base template with the name `base.html` with blocks named `title` and `content`. It includes a button to edit the page in the admin site if the current user is a staff member.