from setuptools import setup

requirements = [
    "Django>3",
    "django-tinymce>=3.1.0",
    "django-ckeditor>=6.0.0",
]

setup(
    install_requires=requirements,
)