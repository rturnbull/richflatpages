DJANGO_SETTINGS_MODULE=tests.settings_tinymce python -m coverage run -m runtests
DJANGO_SETTINGS_MODULE=tests.settings_ckeditor python -m coverage run -a -m runtests
DJANGO_SETTINGS_MODULE=tests.settings_ckeditor_uploader python -m coverage run -a -m runtests
python -m coverage run -a runtestsnowidget.py
coverage html
coverage report