#!/usr/bin/env python
# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import

import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner


def run_tests(*test_args):
    if not test_args:
        test_args = ['tests']

    settings_modules_allowed = [
        'tests.settings_ckeditor',
        'tests.settings_ckeditor_uploader',
        'tests.settings_tinymce',
        'tests.settings_none',
    ]
    if 'DJANGO_SETTINGS_MODULE' in os.environ and os.environ['DJANGO_SETTINGS_MODULE'] not in settings_modules_allowed:
        raise Exception(f"Django settings module '{os.environ['DJANGO_SETTINGS_MODULE']}' not allowed. Choose one of {settings_modules_allowed}.")

    if 'DJANGO_SETTINGS_MODULE' not in os.environ:
        os.environ['DJANGO_SETTINGS_MODULE'] = settings_modules_allowed[0]

    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    failures = test_runner.run_tests(test_args)
    sys.exit(bool(failures))


if __name__ == '__main__':
    run_tests(*sys.argv[1:])
