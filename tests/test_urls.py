from django.test import TestCase
from django.urls import resolve


class TestURLs(TestCase):

    def test_urls(self):
        view, args, kwargs = resolve("/hi.html")

        self.assertIn("flatpage", str(view))
