from tests.settings_base import *

INSTALLED_APPS += [
    'ckeditor',    
    "ckeditor_uploader",
    "richflatpages",    
]


MEDIA_ROOT = ""
CKEDITOR_UPLOAD_PATH = ""